<?php

  /*
	Plugin Name: Webmato: Posts Preview Widget
	Description: 'webmato/page-preview-widget_before-title-label', 'webmato/posts-preview-widget_after-title-label', 'webmato/posts-preview-widget_title_container_class' = ['webmato__posts_preview_widget-heading'], 'webmato/posts-preview-widget_title-container-tag' = [h2]
	Version: 1.0.0
	Author: Tomas Hrda
	Author URI: http://www.webmato.net
	Repository:
	Text Domain: webmato__posts_preview_widget
	License: MIT
	*/

  /*************************************************************************************************
   *                                     CREATE WIDGET
   ************************************************************************************************/
  class webmato__posts_preview_widget extends WP_Widget {
    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/
    /**
     * @var string $base_html_class
     */
    var $base_html_class = 'webmato-posts-preview-widget';

    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    function __construct(){
      parent::__construct(
        'webmato__posts_preview_widget',
        __('Posts Preview', 'webmato__posts_preview_widget'),
        [
          'description' => __('Panel displays preview (excerpt) of last posts.', 'webmato__posts_preview_widget'),
          'classname'   => $this->base_html_class,
        ]
      );
    }

    /***********************************************************************************************
     *                                     Widget Frontend
     **********************************************************************************************/
    public function widget($args, $instance){
      $posts_query = new WP_Query([
        'post_type'      => 'post',
        'cat'            => $instance['select_category'],
        'order'          => 'DESC',
        'orderby'        => 'date',
        'posts_per_page' => $instance['number_of_posts'],
      ]);
      $category_name = get_the_category_by_ID($instance['select_category']);
      $category_url = get_category_link($instance['select_category']);
      $title = apply_filters('widget_title', $category_name);
      $before_title_label = apply_filters('webmato/posts-preview-widget_before-title-label', null);
      $after_title_label = apply_filters('webmato/posts-preview-widget_after-title-label', null);
      $title_container_custom_class = apply_filters('webmato/posts-preview-widget_title_container_class', null);
      $title_container_tag = apply_filters('webmato/posts-preview-widget_title-container-tag', null);
      $title_custom_class = apply_filters('webmato/posts-preview-widget_title_label_class', null);

      if ( !$title_container_custom_class){
        $title_container_custom_class = '';
      }

      if ( !$title_container_tag){
        $title_container_tag = 'h2';
      }

      if ( !$title_custom_class){
        $title_custom_class = '';
      }

      ?>
      <!-- WEBMATO.NET - POST PREVIEW WIDGET -->
      <?php

      echo $args['before_widget'];
      ?>
      <section id="<?= $this->lastElementOfUrl($category_url); ?>"
               class="<?= $this->base_html_class . '__container'; ?>">

        <!-- WEBMATO.NET - POST PREVIEW WIDGET - HEADER -->
        <?php
          if ( !empty($title)){
            ?>
            <header>
              <?= '<' . $title_container_tag . ' class="entry-title ' . $this->base_html_class . '__title-container ' . $title_container_custom_class . '">' ?>
              <a href="<?= $category_url ?>">
                <?= $before_title_label ?>
                <span
                  class="<?= $title_custom_class . ' ' . $this->base_html_class . '__title'; ?>">
                  <?= $title ?>
                </span>
                <?= $after_title_label ?>
              </a>
              <?= '</' . $title_container_tag . '>' ?>
            </header>
            <?php
          }
        ?>
        <!-- WEBMATO.NET - POST PREVIEW WIDGET - HEADER -->


        <ul class="nav <?= $this->base_html_class . '__posts-list'; ?>">
          <?php while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
            <li class="menu-item <?= $this->base_html_class . '__posts-list-item'; ?>">

              <!-- WEBMATO.NET - POST PREVIEW WIDGET - POST CONTENT -->
              <article
                id="<?= strtolower(esc_attr($category_name . '_-_' . $this->lastElementOfUrl(get_permalink()))); ?>"
                class="<?= $this->base_html_class . '__post-container'; ?>">
                <header>
                  <h3 class="entry-title <?= $this->base_html_class . '__post-heading'; ?>">
                    <!-- <a href="--><?php //the_permalink(); ?><!--" -->
                    <!-- class="webmato-posts-preview-widget__post-heading-link"> -->
                    <?php get_the_title() ? the_title() : the_ID(); ?>
                    <!-- </a> -->
                  </h3>
                </header>
                <div class="entry-summary <?= $this->base_html_class . '__post-excerpt'; ?>">
                  <?= get_the_excerpt(); ?>
                </div>
              </article>
              <!-- /WEBMATO.NET - POST PREVIEW WIDGET - POST CONTENT -->

            </li>
          <?php endwhile; ?>
        </ul>


        <footer>
        <span class="<?= $this->base_html_class . '__link-more link-more'; ?>">
          <a href="<?= $category_url ?>">
          <?= __('more', 'webmato__posts_preview_widget') ?>
          </a>
        </span>
        </footer>


      </section>

      <?php
      echo $args['after_widget'];
      ?>
      <!-- /WEBMATO.NET - POST PREVIEW WIDGET -->
      <?php
      wp_reset_postdata();
    }

    /***********************************************************************************************
     *                                     Widget Backend
     **********************************************************************************************/
    public function form($instance){
      $categories = get_categories();
      ?>
      <p>
        <label for="<?php echo $this->get_field_id('select_category'); ?>">
          <?php _e('Select Category of the Posts to display:', 'webmato__posts_preview_widget'); ?>
        </label>
        <select
          class="widefat"
          id="<?php echo $this->get_field_id('select_category'); ?>"
          name="<?php echo $this->get_field_name('select_category'); ?>"
        >
          <?php
            foreach ($categories as $category){
              $selected = '';
              if (array_key_exists('select_category', $instance)){
                $selected = selected($instance['select_category'], $category->term_id, false);
              }
              echo "<option value='$category->term_id' $selected>$category->name</option>";
            }
          ?>
        </select>

        <label for="<?php echo $this->get_field_id('number_of_posts'); ?>">
          <?php _e('Number of posts to display:', 'webmato__posts_preview_widget'); ?>
        </label>
        <?php
          $number_of_posts = 3;
          if (array_key_exists('select_category', $instance)){
            $number_of_posts = $instance['number_of_posts'];
          }
        ?>
        <input
          type="number"
          step="1"
          class="widefat"
          id="<?php echo $this->get_field_id('number_of_posts'); ?>"
          name="<?php echo $this->get_field_name('number_of_posts'); ?>"
          value="<?= $number_of_posts ?>"
        >
      </p>
      <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance){
      $instance['select_category'] = $new_instance['select_category'];
      $instance['number_of_posts'] = strip_tags($new_instance['number_of_posts']);

      return $instance;
    }

    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/
    /**
     * @param {String} $url
     *
     * @return string
     */
    function lastElementOfUrl($url){
      if (substr($url, - 1) === '/'){
        $url = rtrim($url, '/');
      }
      $url_array = explode('/', $url);

      return end($url_array);
    }

  }


  /*************************************************************************************************
   *                                     REGISTER WIDGET
   ************************************************************************************************/
  // Register and load the widget
  add_action('widgets_init', function(){
    register_widget('webmato__posts_preview_widget');
  });

  // Load localization files
  add_action('plugins_loaded', function(){
    $plugin_dir = plugin_basename(dirname(__FILE__));
    $basename = basename($plugin_dir);
    $lang_dir = $basename . '/lang/';

    load_plugin_textdomain('webmato__posts_preview_widget', false, $lang_dir);
  });

